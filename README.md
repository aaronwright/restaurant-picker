## Restaurant Picker

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

Can't decide which restaurant to go to tonight? Fire up Restaurant Picker, and let your computer decide.


This is a fun little React app I built as a refresher project to re-learn React. 
