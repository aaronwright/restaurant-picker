import React, { Component } from 'react';



class Header extends Component{

    render() {
      return (
        <div className="header">
            <div className="container">
            <h1>Indecision</h1>
            <p>Put your life in the hands of a computer</p>
          </div>
        </div>
      )
    }
}


export default Header;
