import React from 'react';
import Modal from 'react-modal'



const OptionModal = (props) => {

      return (
        <div>
          <Modal
            isOpen={!!props.selectedOption}
            contentLabel={"Selected Restaurant"}
            onRequestClose={props.closeModal}
            >
            <h3>{props.selectedOption && <p>{props.selectedOption}</p>}</h3>
            <button onClick={props.closeModal}>CLOSE</button>
          </Modal>
        </div>
      )
}


export default OptionModal;
