import React from 'react';



const Option = (props) => {

      return (
        <div>
          <div className="option">
          <h3>{props.option} </h3>
            {props.favorites.indexOf(props.option) === -1 && <button className="button md-button fav-button" onClick={() => {props.handleAddOptionToFavorites(props.option)}}>FAV IT</button>}
            <button className="sm-button md-button remove-btn" onClick={ () => props.handleDeleteOption(props.option)}>REMOVE</button>
          </div>
        </div>
      )
}


export default Option;
