import React from 'react';
import Option from './Option';


const Options = (props) => {

      return (
        <div>
          <div className="widget-header">
            <h2>Restaurant Choices</h2>
            <button className="md-button button" onClick={props.handleDeleteOptions}>Clear All Options</button>
          </div>

          {props.options.length === 0 && <p>Add a restaurant to get started.</p>}
          {props.options.map((option) => {
                  return  <Option
                            handleAddOptionToFavorites={props.handleAddOptionToFavorites}
                            handleDeleteOption={props.handleDeleteOption}
                            key={option}
                            favorites={props.favorites}
                            option={option} />
          })
        }

        </div>
      )
}


export default Options;
