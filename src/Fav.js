import React from 'react';



const Fav = (props) => {

      return (
        <div>
          <div className="favorite">
            <p>{props.favorite}</p>
            <button className="sm-button sm-fav-button" onClick={() => props.handleAddOneFav(props.favorite)}>+</button>
            <button className="sm-button" onClick={() => props.handleDeleteOneFav(props.favorite)}>-</button>
          </div>
        </div>
      )
}


export default Fav;
