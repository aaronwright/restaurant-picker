import React, { Component } from 'react';
import Fav from './Fav';


class Navbar extends Component {

    constructor(props) {
        super(props);

        this.state = {
          showMenu: false
        }
        this.getFavorites = this.getFavorites.bind(this);
        this.onShowMenu = this.onShowMenu.bind(this);



    }


    getFavorites() {
        if (this.state.showMenu) {
            return (
              <div>
              <button className="md-button add-all-favs-btn" onClick={this.props.handleAddAllFavs}>Add All Favorites</button>
                {this.props.favorites.map((fav) => {
                  return <Fav key={fav} handleAddOneFav={this.props.handleAddOneFav} handleDeleteOneFav={this.props.handleDeleteOneFav} favorite={fav} />;
                })
              }
              </div>
            )
        }
        return undefined;
    }

    onShowMenu() {
        this.setState((prevState) => {
             return {showMenu: !prevState.showMenu}
        });
    }


    render() {
      return (
        <div>
        <button className="sm-button fav-dropdown-btn" onClick={this.onShowMenu}>Favorites</button>

        <div className="favs-container">
        {this.state.showMenu &&
        <div className="favs">
        {this.getFavorites()}
        </div>
        }
        </div>
        </div>
      )
    }
}


export default Navbar;
