import React, { Component } from 'react';



const Content = (props) => {

  let count = 0;

  var user = {
    age: 27,
    name: 'Aaron',
    location: 'Richmond, VA'
  }

//if an exppression evaluates to undefined, nothing will show up
  const getLocation = (location) => {
    if(location) {
        return <p>Location: {location}</p>
    } else {
      return undefined;

    }
  }

  const addOne = () => {
    count = count + 1;
    console.log(count);
  }

  const minusOne = () => {
    count = count + 1;
    console.log('minus');
  }

  const reset = () => {
    console.log('reset');
  }

  var template = (
    <div className="content">
        <h1>Count: {count}</h1>
        <h2>Name: {user.name ? user.name : 'Anonymous'}</h2>
        {user.age >= 18 && <p>Age: {user.age} </p>}
        {getLocation(user.location)}

        <button onClick={addOne}>+1</button>
        <button onClick={minusOne}>-1</button>
        <button onClick={reset}>reset</button>

    </div>
  )
    return template;

}



export default Content;
