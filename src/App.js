import React, { Component } from 'react';
import './App.css';
import Header from './Header';
import Action from './Action';
import Options from './Options';
import AddOption from './AddOption';
import Navbar from './Navbar';
import OptionModal from './OptionModal';





class App extends Component {
  constructor() {
      super();

      this.state = {
        options: [],
        selectedOption: undefined,
        favorites: []
      }

      this.addOption = this.addOption.bind(this);
      this.handleDeleteOptions = this.handleDeleteOptions.bind(this);
      this.handlePick = this.handlePick.bind(this);
      this.handleAddOption = this.handleAddOption.bind(this);
      this.handleDeleteOption = this.handleDeleteOption.bind(this);
      this.handleAddAllFavs = this.handleAddAllFavs.bind(this);
      this.handleAddOneFav = this.handleAddOneFav.bind(this);
      this.handleDeleteOneFav = this.handleDeleteOneFav.bind(this);

      this.handleAddOptionToFavorites = this.handleAddOptionToFavorites.bind(this);

  }

  componentDidMount() {

    try {
      const json = localStorage.getItem('options');
      const options = JSON.parse(json);

      const jsonFavs = localStorage.getItem('favorites');
      const favs = JSON.parse(jsonFavs);


      if (options) {
        this.setState(() => {
          return {options: options};
        });
      }

      if (favs) {
        this.setState(() => {
          return {favorites: favs};
        });
      }
    } catch (e) {
        //ERROR, do nothing
        console.log(e);

    }
  }

  componentDidUpdate(prevProps, prevState) {
      if(prevState.options.length !== this.state.options.length) {
        const json = JSON.stringify(this.state.options);

        localStorage.setItem('options', json);
      }


      if(prevState.favorites.length !== this.state.favorites.length) {
          const jsonFavs = JSON.stringify(this.state.favorites);

          localStorage.setItem('favorites', jsonFavs);
      }
  }

  addOption = () => {

  }

  closeModal = () => {
    this.setState({selectedOption: undefined});
  }

  handleDeleteOptions() {
      this.setState({options: []});
  }

  handleAddAllFavs() {
      const favsNotInState = this.state.favorites.filter((fav)=> {
          if (this.state.options.indexOf(fav) === -1) {
            return fav;
          }
          return undefined;
      });
    this.setState((prevState) => {
      return  {options: this.state.options.concat(favsNotInState)};
    })
  }

  handleAddOneFav(fav) {
    this.setState((prevState) => {
      return {options: prevState.options.concat(fav)};
    })
  }

  handleDeleteOneFav(favToDelete) {
    this.setState((prevState) => ({
        favorites: prevState.favorites.filter((fav) => {
          return favToDelete !== fav;
        })
    }))

  }

  handlePick() {
      const randomNum = Math.floor(Math.random() * this.state.options.length);
      const option = this.state.options[randomNum];
      this.setState({options: [], selectedOption: option});
  }

  handleAddOption(option) {
    if(!option) {
      return "Enter a valid restaurant to add item";
    } else if (this.state.options.indexOf(option) > -1) {
      return "This restaurant already exists.";
    }

    this.setState((prevState) => {
        return {
          options: prevState.options.concat([option])
        }
    })
  }

  handleDeleteOption(optionToRemove) {
    this.setState((prevState) => ({
        options: prevState.options.filter((option) => {
          return optionToRemove !== option;
        })
    }))
  }

  handleAddOptionToFavorites(optionToAdd) {
    if (this.state.favorites.indexOf(optionToAdd) > -1) {
      return "Favorite already exists";
    }
    this.setState({favorites: this.state.favorites.concat(optionToAdd) });
  }

  render() {

    return (
      <div className="App">
      <Header />
      <Navbar handleDeleteOneFav={this.handleDeleteOneFav} handleAddOneFav={this.handleAddOneFav} handleAddAllFavs={this.handleAddAllFavs} favorites={this.state.favorites}/>

        <div className="container">
          <Action handlePick={this.handlePick} hasOptions={this.state.options.length > 0} />
          <div className="widget">
            <Options
              handleAddOptionToFavorites={this.handleAddOptionToFavorites}
              handleDeleteOption={this.handleDeleteOption}
              handleDeleteOptions={this.handleDeleteOptions}
              favorites={this.state.favorites}
              options={this.state.options} />
            <AddOption handleAddOption={this.handleAddOption} />
          </div>
        </div>

          <OptionModal closeModal={this.closeModal} selectedOption={this.state.selectedOption} />
      </div>
    );
  }
}

export default App;
